function counterFactory(num=0){
    let counter=num;
    return {
        increment:function(value=1){
            return counter += value;
        },
        decrement:function(value=1){
            return counter=counter-value;
        }
    }
}
module.exports=counterFactory;