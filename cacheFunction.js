function cacheFunction(cb){
    let cache={};

    return function inner(...args){
        console.log(args)
        let is_key_in_cache = false;
        for (let key in cache){
            if (key == args){
                is_key_in_cache=true
                return cache[key]
            }
        }
        if (is_key_in_cache===false){
            result = cb(...args)
            cache[args]=result;
            return result
        }
    }
}
module.exports = cacheFunction;