function limitFunctionCallCount(cb,n){
    let count=0;
    function FunctionCall(...values){
        count += 1;
        if (count>n){
            return null;
        }
        else{
            return cb(...values);
        }
    }
    return FunctionCall;
}
module.exports=limitFunctionCallCount;