let limitFunctionCallCount=require('../limitFunctionCallCount')
function sum(value1,value2){
    return value1+value2;
}
let output=limitFunctionCallCount(sum,2);
console.log(output(3,4));
console.log(output(2,5));
console.log(output(100,37))